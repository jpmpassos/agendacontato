/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.idioma;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JP1
 */
public class Linguagem {

    private List<MapaAplicacao> linguagens;

    private static Linguagem instance;

    private Linguagem() {
        linguagens = new ArrayList<>();
        MapaAplicacao mapaAplicacao = new MapaAplicacao();
        mapaAplicacao.setIdioma("br");
        mapaAplicacao.setPrincipalTitulo("Mensagens Recebidas");
        mapaAplicacao.setMensagememptyrecado("Nenhum recado encontrado na consulta");
        mapaAplicacao.setPrincipalbutton1("Visualizar mensagem");
        mapaAplicacao.setPrincipallabel1("Não Lido");
        mapaAplicacao.setMensagememptyrecado("Marcar Como não Lida");
        mapaAplicacao.setPrincipalcolunaremetente("Remetente");
        mapaAplicacao.setLabeldata("Data");
        mapaAplicacao.setPrincipaltitulosalvarrecado("Salvar Recado");
        mapaAplicacao.setButtonsalvar("Salvar");
        mapaAplicacao.setButtonvoltar("Voltar");
        mapaAplicacao.setHeaderlabelconfirmacao("Confirmação");
        mapaAplicacao.setMensagemconfirmacaovoltar("Todas as alterações serão perdidas! Confirma sair da tela?");
        mapaAplicacao.setPrincipallabelcontato("Contato");
        mapaAplicacao.setPrincipalmensagem4("Nenhum contato selecionado!");
        mapaAplicacao.setMensagememptycontato("Nenhum contato encontrado na consulta!");
        mapaAplicacao.setPrincipallabeltexto("Texto");
        mapaAplicacao.setPrincipalplaceholder1("Texto do recado");
        mapaAplicacao.setPrincipaltitulorecadosenviados("Lista de Recados Enviados");
        mapaAplicacao.setLabelrecado("Recado");
        mapaAplicacao.setLabelnome("Nome");
        mapaAplicacao.setLabelcontato("Contato");
        mapaAplicacao.setButtonnovorecado("Novo Recado");
        mapaAplicacao.setLabelrecadosenviados("Recados Enviados");
        mapaAplicacao.setButtonrelatorioderecados("Relatórios de Recados");
        mapaAplicacao.setMensagemsaudacaotemplate("Bem Vindo");
        mapaAplicacao.setPrincipalmensagemnumerorecados("Quantidade de recados recebidos hoje ");

        linguagens.add(mapaAplicacao);
        
        mapaAplicacao = new MapaAplicacao();
        mapaAplicacao.setIdioma("eng");
        mapaAplicacao.setPrincipalTitulo("Received messages");
        mapaAplicacao.setMensagememptyrecado("No messages found in the query");
        mapaAplicacao.setPrincipalbutton1("View message");
        mapaAplicacao.setPrincipallabel1("Not read");
        mapaAplicacao.setMensagememptyrecado("Mark as unread");
        mapaAplicacao.setPrincipalcolunaremetente("Sender");
        mapaAplicacao.setLabeldata("Date");
        mapaAplicacao.setPrincipaltitulosalvarrecado("Save Message");
        mapaAplicacao.setButtonsalvar("Save");
        mapaAplicacao.setButtonvoltar("Back");
        mapaAplicacao.setHeaderlabelconfirmacao("Confirmation");
        mapaAplicacao.setMensagemconfirmacaovoltar("All changes will be lost! Do you confirm you leave the screen?");
        mapaAplicacao.setPrincipallabelcontato("Contact");
        mapaAplicacao.setPrincipalmensagem4("No contacts selected!");
        mapaAplicacao.setMensagememptycontato("No contacts found in the query!");
        mapaAplicacao.setPrincipallabeltexto("Text");
        mapaAplicacao.setPrincipalplaceholder1("Message text");
        mapaAplicacao.setPrincipaltitulorecadosenviados("List of Sent Messages");
        mapaAplicacao.setLabelrecado("Message");
        mapaAplicacao.setLabelnome("Name");
        mapaAplicacao.setLabelcontato("Contact");
        mapaAplicacao.setButtonnovorecado("New Message");
        mapaAplicacao.setLabelrecadosenviados("View Sent Messages");
        mapaAplicacao.setButtonrelatorioderecados("Report Abuse");
        mapaAplicacao.setMensagemsaudacaotemplate("Welcome");
        mapaAplicacao.setPrincipalmensagemnumerorecados("Number of messages received today ");

        linguagens.add(mapaAplicacao);
    }

    public static MapaAplicacao carregarLiguagem(String str) {
        if (instance == null) {
            instance = new Linguagem();
        }

        for (MapaAplicacao linguagem : instance.linguagens) {
            if (linguagem.getIdioma().equals(str)) {
                return linguagem;
            }
        }
        return null;
    }

    public List<MapaAplicacao> getLinguagens() {
        return linguagens;
    }

    public void setLinguagens(List<MapaAplicacao> linguagens) {
        this.linguagens = linguagens;
    }

}
