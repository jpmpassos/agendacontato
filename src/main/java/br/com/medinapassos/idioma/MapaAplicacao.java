/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.idioma;

/**
 *
 * @author JP1
 */
public class MapaAplicacao {

    private String idioma;
    private String principalTitulo;
    private String principalbutton1;

    private String mensagememptyrecado;
    private String mensagememptycontato;
    private String principalmensagem2;
    private String principallabel1;
    private String principalmensagem3;
    private String principalcolunaremetente;
    private String labeldata;
    private String principaltitulosalvarrecado;
    private String principallabelcontato;
    private String principalmensagem4;
    private String principallabeltexto;
    private String principalplaceholder1;
    private String principaltitulorecadosenviados;
    private String principalmensagemnumerorecados;

    private String labelnome;
    private String labelcontato;
    private String labelrecadosenviados;
    private String labelrecado;
    private String buttonsalvar;
    private String buttonvoltar;
    private String buttonnovorecado;
    private String buttonrelatorioderecados;
    private String headerlabelconfirmacao;
    private String mensagemconfirmacaovoltar;
    private String mensagemsaudacaotemplate;

    public String getPrincipalmensagemnumerorecados() {
        return principalmensagemnumerorecados;
    }

    public void setPrincipalmensagemnumerorecados(String principalmensagemnumerorecados) {
        this.principalmensagemnumerorecados = principalmensagemnumerorecados;
    }

    public String getMensagemsaudacaotemplate() {
        return mensagemsaudacaotemplate;
    }

    public void setMensagemsaudacaotemplate(String mensagemsaudacaotemplate) {
        this.mensagemsaudacaotemplate = mensagemsaudacaotemplate;
    }

    public String getLabelrecadosenviados() {
        return labelrecadosenviados;
    }

    public void setLabelrecadosenviados(String labelrecadosenviados) {
        this.labelrecadosenviados = labelrecadosenviados;
    }

    public String getButtonrelatorioderecados() {
        return buttonrelatorioderecados;
    }

    public void setButtonrelatorioderecados(String buttonrelatorioderecados) {
        this.buttonrelatorioderecados = buttonrelatorioderecados;
    }

    public String getButtonnovorecado() {
        return buttonnovorecado;
    }

    public void setButtonnovorecado(String buttonnovorecado) {
        this.buttonnovorecado = buttonnovorecado;
    }

    public String getLabelcontato() {
        return labelcontato;
    }

    public void setLabelcontato(String labelcontato) {
        this.labelcontato = labelcontato;
    }

    public String getLabelnome() {
        return labelnome;
    }

    public void setLabelnome(String labelnome) {
        this.labelnome = labelnome;
    }

    public String getMensagememptycontato() {
        return mensagememptycontato;
    }

    public void setMensagememptycontato(String mensagememptycontato) {
        this.mensagememptycontato = mensagememptycontato;
    }

    public String getLabelrecado() {
        return labelrecado;
    }

    public void setLabelrecado(String labelrecado) {
        this.labelrecado = labelrecado;
    }

    public String getPrincipaltitulorecadosenviados() {
        return principaltitulorecadosenviados;
    }

    public void setPrincipaltitulorecadosenviados(String principaltitulorecadosenviados) {
        this.principaltitulorecadosenviados = principaltitulorecadosenviados;
    }

    public String getPrincipalplaceholder1() {
        return principalplaceholder1;
    }

    public void setPrincipalplaceholder1(String principalplaceholder1) {
        this.principalplaceholder1 = principalplaceholder1;
    }

    public String getPrincipallabeltexto() {
        return principallabeltexto;
    }

    public void setPrincipallabeltexto(String principallabeltexto) {
        this.principallabeltexto = principallabeltexto;
    }

    public String getPrincipalmensagem4() {
        return principalmensagem4;
    }

    public void setPrincipalmensagem4(String principalmensagem4) {
        this.principalmensagem4 = principalmensagem4;
    }

    public String getPrincipallabelcontato() {
        return principallabelcontato;
    }

    public void setPrincipallabelcontato(String principallabelcontato) {
        this.principallabelcontato = principallabelcontato;
    }

    public String getMensagemconfirmacaovoltar() {
        return mensagemconfirmacaovoltar;
    }

    public void setMensagemconfirmacaovoltar(String mensagemconfirmacaovoltar) {
        this.mensagemconfirmacaovoltar = mensagemconfirmacaovoltar;
    }

    public String getHeaderlabelconfirmacao() {
        return headerlabelconfirmacao;
    }

    public void setHeaderlabelconfirmacao(String headerlabelconfirmacao) {
        this.headerlabelconfirmacao = headerlabelconfirmacao;
    }

    public String getButtonvoltar() {
        return buttonvoltar;
    }

    public void setButtonvoltar(String buttonvoltar) {
        this.buttonvoltar = buttonvoltar;
    }

    public String getButtonsalvar() {
        return buttonsalvar;
    }

    public void setButtonsalvar(String buttonsalvar) {
        this.buttonsalvar = buttonsalvar;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getPrincipalTitulo() {
        return principalTitulo;
    }

    public void setPrincipalTitulo(String principalTitulo) {
        this.principalTitulo = principalTitulo;
    }

    public String getMensagememptyrecado() {
        return mensagememptyrecado;
    }

    public void setMensagememptyrecado(String mensagememptyrecado) {
        this.mensagememptyrecado = mensagememptyrecado;
    }

    public String getPrincipalmensagem2() {
        return principalmensagem2;
    }

    public void setPrincipalmensagem2(String principalmensagem2) {
        this.principalmensagem2 = principalmensagem2;
    }

    public String getPrincipallabel1() {
        return principallabel1;
    }

    public void setPrincipallabel1(String principallabel1) {
        this.principallabel1 = principallabel1;
    }

    public String getPrincipalmensagem3() {
        return principalmensagem3;
    }

    public void setPrincipalmensagem3(String principalmensagem3) {
        this.principalmensagem3 = principalmensagem3;
    }

    public String getPrincipalbutton1() {
        return principalbutton1;
    }

    public void setPrincipalbutton1(String principalbutton1) {
        this.principalbutton1 = principalbutton1;
    }

    public String getPrincipalcolunaremetente() {
        return principalcolunaremetente;
    }

    public void setPrincipalcolunaremetente(String principalcolunaremetente) {
        this.principalcolunaremetente = principalcolunaremetente;
    }

    public String getLabeldata() {
        return labeldata;
    }

    public void setLabeldata(String labeldata) {
        this.labeldata = labeldata;
    }

    public String getPrincipaltitulosalvarrecado() {
        return principaltitulosalvarrecado;
    }

    public void setPrincipaltitulosalvarrecado(String principaltitulosalvarrecado) {
        this.principaltitulosalvarrecado = principaltitulosalvarrecado;
    }

}
