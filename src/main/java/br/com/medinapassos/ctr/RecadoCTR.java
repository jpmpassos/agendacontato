/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.ctr;

import br.com.medinapassos.base.Recado;
import br.com.medinapassos.dao.RecadoDAO;
import br.com.medinapassos.util.DAOFactory;
import java.util.List;

/**
 *
 * @author João Paulo
 */
public class RecadoCTR {

    private final RecadoDAO recadoDAO;

    public RecadoCTR(RecadoDAO recadoDAO) {
        this.recadoDAO = recadoDAO;
    }

    public RecadoCTR() {
        this.recadoDAO = DAOFactory.criarRecadoDAO();
    }

    public void salvar(Recado recado) {
        this.recadoDAO.salvar(recado);
    }

    public void excluir(Recado recado) {
        this.recadoDAO.excluir(recado);
    }

    public Recado carregar(Integer codigo) {
        return this.recadoDAO.carregar(codigo);
    }

    public List<Recado> listar() {
        return this.recadoDAO.listar();
    }

    public List<Recado> listarLidosEAntigos() {
        return this.recadoDAO.listarLidosEAntigos();
    }

    public List<Recado> listarRemetente(Integer remetenteid) {
        return this.recadoDAO.listarRemetente(remetenteid);
    }

    public List<Recado> listarContato(Integer contatoid) {
        return this.recadoDAO.listarContato(contatoid);
    }

    public void DeletarRecadosAntigos() {
        this.recadoDAO.DeletarRecadosAntigos();
    }

    public Integer numerosrecadosdiario(Integer contatoid) {
        return this.recadoDAO.numerosrecadosdiario(contatoid);
    }
}
