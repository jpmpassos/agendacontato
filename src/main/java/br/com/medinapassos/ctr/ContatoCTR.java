/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.ctr;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.dao.ContatoDAO;
import br.com.medinapassos.util.DAOFactory;
import java.util.List;

/**
 *
 * @author João Paulo
 */
public class ContatoCTR {

    private final ContatoDAO contatoDAO;

    public ContatoCTR() {
        this.contatoDAO = DAOFactory.criarContatoDAO();
    }

    public void salvar(Contato contato) {
        this.contatoDAO.salvar(contato);
    }

    public void excluir(Contato contato) {
        this.contatoDAO.excluir(contato);
    }

    public Contato carregar(Integer codigo) {
        return this.contatoDAO.carregar(codigo);
    }

    public List<Contato> listar() {
        return this.contatoDAO.listar();
    }

    public Integer countLazyFilter(String strQuery) {
        return this.contatoDAO.countLazyFilter(strQuery);
    }

    public List<Contato> listarLazyFilter(String strQuery) {
        return this.contatoDAO.listarLazyFilter(strQuery);
    }
    
    public Contato recuperar(String login){
        return this.contatoDAO.recuperar(login);
    }

    public Boolean verificarDisponivilidadeLogin(String login, Integer contatoid){
        return this.contatoDAO.verificarDisponivilidadeLogin(login, contatoid);
    }

}
