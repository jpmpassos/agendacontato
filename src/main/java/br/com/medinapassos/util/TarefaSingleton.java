/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

import java.util.Timer;

/**
 *
 * @author JP1
 */
public class TarefaSingleton {

    private static TarefaSingleton instancia;

    private TarefaSingleton() {
        Timer timer = new Timer(true);
        ApagarRecados apagar = new ApagarRecados();
        timer.schedule(apagar, 0, 3600000); //3600000
    }

    public static void agendar() {
        if (instancia == null) {
            instancia = new TarefaSingleton();
        }
    }

}
