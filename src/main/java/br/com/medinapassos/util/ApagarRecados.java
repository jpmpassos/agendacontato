/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

/**
 *
 * @author JP1
 */
import br.com.medinapassos.base.Recado;
import br.com.medinapassos.ctr.RecadoCTR;
import br.com.medinapassos.dao.RecadoDAOImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import java.util.TimerTask;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ApagarRecados extends TimerTask {

    private static final Logger logger = Logger.getLogger("Log Agenda re Recados");

    public ApagarRecados() {
        String atualDir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/")
                + "WEB-INF/classes/log4j.properties";
        
        File f = new File(atualDir);
        f.getAbsolutePath();
        if (f.exists()) {
            try {
                InputStream inStreamLog4j = new FileInputStream(f);
                Properties propertiesLog4j = new Properties();

                propertiesLog4j.load(inStreamLog4j);
                PropertyConfigurator.configure(propertiesLog4j);
            } catch (Exception e) {
                e.printStackTrace();
                BasicConfigurator.configure();
            }
        } else {
            BasicConfigurator.configure();
        }
    }

    @Override
    public void run() {
        //Logger logger = LogManager.getLogger("Simples");
        List<Recado> recados = null;
        try {
            RecadoDAOImpl recadoDAOImpl = new RecadoDAOImpl();
            recadoDAOImpl.setSession(HibernateUtil.getSessionFactory().openSession());
            RecadoCTR recadoCTR = new RecadoCTR(recadoDAOImpl);
            try {
                recadoDAOImpl.getSession().beginTransaction();

                recados = recadoCTR.listarLidosEAntigos();

                recadoCTR.DeletarRecadosAntigos();
                recadoDAOImpl.getSession().getTransaction().commit();
                recadoDAOImpl.getSession().close();
            } catch (Exception e) {
                recadoDAOImpl.getSession().getTransaction().rollback();
                recadoDAOImpl.getSession().close();
                logger.info("Erro ao apagar recados. ERRO: " + e.getLocalizedMessage());
                return;
            }
        } catch (Exception e) {
            logger.info("Erro ao apagar recados. ERRO: " + e.getLocalizedMessage());
            return;
        }

        if (recados != null && recados.size() > 0) {
            logger.info("Foi executado com sucesso a função de Apagar recados. E apagados "
                    + ((Integer) recados.size()).toString() + " recados.");
        }
    }

}
