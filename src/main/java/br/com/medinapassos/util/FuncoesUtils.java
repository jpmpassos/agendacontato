/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.base.Recado;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author João Paulo
 */
public class FuncoesUtils {

    public static String converteSHA256(String senha) {
        String sen = "";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
        sen = hash.toString(16);
        return sen;
    }

    public static void enviarMensagemTela(String msg, Integer tipo, String fecharJanela, String abrirJanela) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        if (msg != null && !msg.trim().isEmpty()) {
            if (null != tipo) {
                switch (tipo) {
                    case 1:
                        //Sucesso
                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", msg);
                        break;
                    case 2:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Cuidado!", msg);
                        break;
                    case 3:
                        message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", msg);
                        break;
                    case 4:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro Grave!", msg);
                        break;
                    case 5:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro!", msg);
                        tipo = 1;
                        break;
                    default:
                        break;
                }
            }

            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        context.addCallbackParam("tipo", tipo);
        context.addCallbackParam("fecharJanela", fecharJanela);
        context.addCallbackParam("abrirJanela", abrirJanela);
    }
    
    public static void enviarMensagemTela(String msg, Integer tipo) {
        FacesMessage message = null;
        if (msg != null && !msg.trim().isEmpty()) {
            if (null != tipo) {
                switch (tipo) {
                    case 1:
                        //Sucesso
                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", msg);
                        break;
                    case 2:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Cuidado!", msg);
                        break;
                    case 3:
                        message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", msg);
                        break;
                    case 4:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro Grave!", msg);
                        break;
                    case 5:
                        message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro!", msg);
                        tipo = 1;
                        break;
                    default:
                        break;
                }
            }

            FacesContext.getCurrentInstance().addMessage(null, message);
        }        
    }
    
    
     public static void redirect(String pag) {
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            String contextPath = externalContext.getRequestContextPath();

            externalContext.redirect(contextPath + pag);
            facesContext.responseComplete();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
     
      public static void preencherContatoRemetenteRecado(List<Recado> recados, List<Contato> contatos) {

        Comparator<Recado> crecado = new Comparator<Recado>() {
            @Override
            public int compare(Recado r1, Recado r2) {
                return r1.getContatoremetenteid().compareTo(r2.getContatoremetenteid());
            }
        };

        Comparator<Contato> ccontato = new Comparator<Contato>() {
            @Override
            public int compare(Contato c1, Contato c2) {
                return c1.getContatoid().compareTo(c2.getContatoid());
            }
        };

        Collections.sort(recados, crecado);
        Collections.sort(contatos, ccontato);

        int x1, x2;

        x1 = 0;
        x2 = 0;
        while (contatos.size() > x1 && recados.size() > x2) {
            if (contatos.get(x1).getContatoid().intValue() == recados.get(x2).getContatoremetenteid().intValue()) {
                recados.get(x2).setContato(contatos.get(x1));            
                x2++;
            } else if (contatos.get(x1).getContatoid().intValue() > recados.get(x2).getContatoremetenteid().intValue()) {
                x2++;
            } else if (contatos.get(x1).getContatoid().intValue() < recados.get(x2).getContatoremetenteid().intValue()) {
                x1++;
            }
        }
    }

}
