/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.activation.DataSource;
import javax.faces.context.FacesContext;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;

/**
 *
 * @author João Paulo
 */
public class RelatoriosUtil {

    private Connection getConnection() throws HibernateException, SQLException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        SessionImplementor sessionImplementor = (SessionImplementor) session;
        return sessionImplementor.getJdbcConnectionAccess().obtainConnection();
    }

    public void exportarPDF(Map parametros, String relatorio, String nomeOut) throws HibernateException, JRException, SQLException, IOException {
        File file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(relatorio));

        JasperPrint jasperPrint = JasperFillManager.fillReport(file.getPath(), parametros, getConnection());

        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + nomeOut + ".pdf");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);

        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public <T> void exportarPDF(Map parametros, String relatorio, String nomeOut, List<T> lista) throws HibernateException, JRException, SQLException, IOException {
        File file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(relatorio));

        JasperPrint jasperPrint = JasperFillManager.fillReport(file.getPath(), parametros, new JRBeanCollectionDataSource(lista));

        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + nomeOut + ".pdf");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);

        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public <T> DataSource exportarPDFEmail(Map parametros, String relatorio, String nomeOut, List<T> lista) throws HibernateException, JRException, SQLException, IOException {
        File file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(relatorio));

        JasperPrint jasperPrint = JasperFillManager.fillReport(file.getPath(), parametros, new JRBeanCollectionDataSource(lista));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
        DataSource aAttachment = new ByteArrayDataSource(baos.toByteArray(), "application/pdf");

        return aAttachment;
    }

    public DataSource exportarPDFEmail(Map parametros, String relatorio, String nomeOut) throws HibernateException, JRException, SQLException, IOException {
        File file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(relatorio));

        JasperPrint jasperPrint = JasperFillManager.fillReport(file.getPath(), parametros, getConnection());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
        DataSource aAttachment = new ByteArrayDataSource(baos.toByteArray(), "application/pdf");

        return aAttachment;
    }

    public void verPDF(Map parametros, String relatorio) throws HibernateException, JRException, SQLException, IOException {
        File file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(relatorio));
        byte[] bytes = JasperRunManager.runReportToPdf(file.getPath(), parametros, getConnection());

        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.setContentType("application/pdf");
        httpServletResponse.setContentLength(bytes.length);

        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();

        servletOutputStream.write(bytes, 0, bytes.length);
        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }
}
