/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

//import br.com.vendasmobile.dao.*;
import br.com.medinapassos.dao.ContatoDAO;
import br.com.medinapassos.dao.ContatoDAOImpl;
import br.com.medinapassos.dao.RecadoDAO;
import br.com.medinapassos.dao.RecadoDAOImpl;

/**
 *
 * @author João Paulo
 */
public class DAOFactory {

    public static ContatoDAO criarContatoDAO() {
        ContatoDAOImpl contatoDAOImpl = new ContatoDAOImpl();
        contatoDAOImpl.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
        return contatoDAOImpl;
    }

    public static RecadoDAO criarRecadoDAO() {
        RecadoDAOImpl recadoDAOImpl = new RecadoDAOImpl();
        recadoDAOImpl.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
        return recadoDAOImpl;
    }
}
