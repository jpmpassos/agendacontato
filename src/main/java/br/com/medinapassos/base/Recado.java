/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.base;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

/**
 *
 * @author JP1
 */
@Audited
@AuditTable(value = "aud_recados")
@Entity
@Table(name = "recados")
public class Recado implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer recadoid;
    @Column(name = "idContato")
    private Integer contatoid;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date horario;
    private String quem;
    @Column(name = "idRemetente")
    private Integer contatoremetenteid;
    private String texto;
    private Boolean indlido;
    @Transient
    private Contato contato;

    public Integer getRecadoid() {
        return recadoid;
    }

    public void setRecadoid(Integer recadoid) {
        this.recadoid = recadoid;
    }

    public Integer getContatoid() {
        return contatoid;
    }

    public void setContatoid(Integer contatoid) {
        this.contatoid = contatoid;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public String getQuem() {
        return quem;
    }

    public void setQuem(String quem) {
        this.quem = quem;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Boolean getIndlido() {
        return indlido;
    }

    public void setIndlido(Boolean indlido) {
        this.indlido = indlido;
    }

    public Integer getContatoremetenteid() {
        return contatoremetenteid;
    }

    public void setContatoremetenteid(Integer contatoremetenteid) {
        this.contatoremetenteid = contatoremetenteid;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Recado() {
    }

    public Recado(Integer recadoid, Integer contatoremetenteid) {
        this.recadoid = recadoid;
        this.contatoremetenteid = contatoremetenteid;
    } 
}
