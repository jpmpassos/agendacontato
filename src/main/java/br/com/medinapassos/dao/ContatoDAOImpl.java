/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.dao;

import br.com.medinapassos.base.Contato;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author João Paulo
 */
public class ContatoDAOImpl implements ContatoDAO {

    private Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void salvar(Contato contato) {
        if (contato.getContatoid() != null) {
            this.session.update(contato);
        } else {
            this.session.save(contato);
        }
    }

    @Override
    public void excluir(Contato contato) {
        this.session.delete(contato);
    }

    @Override
    public Contato carregar(Integer codigo) {
        return (Contato) this.session.get(Contato.class, codigo);
    }

    @Override
    public List<Contato> listar() {
        return this.session.createCriteria(Contato.class).list();
    }

    @Override
    public Integer countLazyFilter(String strQuery) {
        Query query = session.createSQLQuery("SELECT count(*) from contatos c " + strQuery);
        BigInteger bi = (BigInteger) query.uniqueResult();
        return bi.intValue();
    }

    @Override
    public List<Contato> listarLazyFilter(String strQuery) {
        Query query = session.createSQLQuery("select * from contatos c "
                + strQuery).addEntity(Contato.class);
        return query.list();
    }

    @Override
    public Contato recuperar(String login) {
        Query resposta = session.createQuery("from Contato where login = :login");
        resposta.setParameter("login", login);
        if (resposta.list().size() == 1) {
            return (Contato) resposta.list().get(0);
        }
        return null;
    }

    @Override
    public Boolean verificarDisponivilidadeLogin(String login, Integer contatoid) {

        Query resposta = null;

        if (contatoid != null) {
            resposta = session.createSQLQuery("select * from contatos where lower(login) = lower(:login) and id <> (:contatoid)");
            resposta.setString("login", login);
            resposta.setInteger("contatoid", contatoid);
        } else {
            resposta = session.createSQLQuery("select * from contatos where lower(login) = lower(:login)");
            resposta.setString("login", login);
        }

        return resposta.list().isEmpty();
    }
}
