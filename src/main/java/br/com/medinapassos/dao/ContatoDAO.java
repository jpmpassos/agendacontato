/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.dao;

import br.com.medinapassos.base.Contato;
import java.util.List;

/**
 *
 * @author João Paulo
 */
public interface ContatoDAO {

    public void salvar(Contato contato);

    public void excluir(Contato contato);

    public Contato carregar(Integer codigo);

    public List<Contato> listar();

    public Integer countLazyFilter(String strQuery);

    public List<Contato> listarLazyFilter(String strQuery);

    public Contato recuperar(String login);

    public Boolean verificarDisponivilidadeLogin(String login, Integer contatoid);
}
