/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.dao;

import br.com.medinapassos.base.Recado;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author João Paulo
 */
public class RecadoDAOImpl implements RecadoDAO {

    private Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void salvar(Recado recado) {
        if (recado.getRecadoid() != null) {
            this.session.update(recado);
        } else {
            this.session.save(recado);
        }
    }

    @Override
    public void excluir(Recado recado) {
        this.session.delete(recado);
    }

    @Override
    public Recado carregar(Integer codigo) {
        return (Recado) this.session.get(Recado.class, codigo);
    }

    @Override
    public List<Recado> listar() {
        return this.session.createCriteria(Recado.class).list();
    }

    @Override
    public List<Recado> listarRemetente(Integer remetenteid) {
        Query query = session.createSQLQuery("select * from recados r where r.idRemetente = :remetenteid ").
                addEntity(Recado.class);
        query.setInteger("remetenteid", remetenteid);
        return query.list();
    }

    @Override
    public Integer numerosrecadosdiario(Integer contatoid) {
        String str
                = "select count(*) contador from recados r \n"
                + " where r.idContato = :contatoid \n"
                + "   and Day(r.gti_created_at) = Day(current_date()) \n"
                + "   and month(r.gti_created_at) = month(current_date()) \n"
                + "   and year(r.gti_created_at) = year(current_date()) ";

        Query query = session.createSQLQuery(str);
        query.setInteger("contatoid", contatoid);  
        
        BigInteger contador = (BigInteger) query.uniqueResult();
        return contador.intValue();
    }

    @Override
    public List<Recado> listarLidosEAntigos() {
        Query query = session.createSQLQuery(
                "select * from recados where indlido and ((current_timestamp()  - gti_modified_at) > (3600 * 2)) "
        ).addEntity(Recado.class);
        return query.list();
    }

    @Override
    public List<Recado> listarContato(Integer contatoid) {
        Query query = session.createSQLQuery("select * from recados r where r.idContato = :contatoid ").
                addEntity(Recado.class);
        query.setInteger("contatoid", contatoid);
        return query.list();
    }

    @Override
    public void DeletarRecadosAntigos() {
        Query query = session.createSQLQuery(
                "delete from recados  where indlido and ((current_timestamp()  - gti_modified_at) > (3600 * 2)) "
        ).addEntity(Recado.class);
        query.executeUpdate();
    }

}
