/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.dao;

import br.com.medinapassos.base.Recado;
import java.util.List;

/**
 *
 * @author João Paulo
 */
public interface RecadoDAO {

    public void salvar(Recado recado);

    public void excluir(Recado recado);

    public Recado carregar(Integer codigo);

    public List<Recado> listar();

    public List<Recado> listarLidosEAntigos();

    public List<Recado> listarRemetente(Integer remetenteid);

    public List<Recado> listarContato(Integer contatoid);

    public void DeletarRecadosAntigos();
    
    public Integer numerosrecadosdiario(Integer contatoid);
}
