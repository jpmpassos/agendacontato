/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.lazy;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.ctr.ContatoCTR;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author João Paulo
 */
public class LazyContatoDataModel extends LazyDataModel<Contato> {

    private static final long serialVersionUID = 2889875020004523098L;

    private List<Contato> datasource;

    @Override
    public Contato getRowData(String rowKey) {
        for (Contato contato : datasource) {
            if (contato.getContatoid().toString().equals(rowKey)) {
                return contato;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Contato contato) {
        return contato.getContatoid();
    }

    @Override
    public List<Contato> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        String strWhere = "";

        if (filters != null && !filters.isEmpty()) {
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                //if (key.equals("nome") || key.equals("nome")) {
                strWhere += " and c." + key + " like('%" + value.toString() + "%')";
//                } else if (key.equals("id")) {
//                    try {
//                        strWhere += " and id = " + ((Integer) Integer.parseInt(value.toString())).toString();
//                    } catch (Exception e) {
//                    }
//                }
            }
        }

        String strSort = "";
        if (sortField != null && !sortField.isEmpty()) {
            String strOrdem = "";

            if (sortOrder == SortOrder.DESCENDING) {
                strOrdem = " desc ";
            }

            strSort = " ORDER BY c." + sortField + strOrdem;
        }

        if (strSort == "") {
            strSort = " ORDER BY id ";
        }

        String strQuery = " where 1 = 1 ";
        strQuery += strWhere;

        ContatoCTR contatoCTR = new ContatoCTR();
        int countData = contatoCTR.countLazyFilter(strQuery);
        this.setRowCount(countData);

        strQuery += strSort;

        String strLimit = "";
        if (pageSize > 0) {
            strLimit = " LIMIT " + String.valueOf(pageSize) + " OFFSET " + String.valueOf(first);
        }

        this.datasource = contatoCTR.listarLazyFilter(strQuery + strLimit);

        return this.datasource;
    }

}
