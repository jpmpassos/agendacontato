/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.bean;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.base.Recado;
import br.com.medinapassos.ctr.ContatoCTR;
import br.com.medinapassos.ctr.RecadoCTR;
import br.com.medinapassos.idioma.Linguagem;
import br.com.medinapassos.idioma.MapaAplicacao;
import br.com.medinapassos.util.FuncoesUtils;
import br.com.medinapassos.util.HibernateUtil;
import br.com.medinapassos.util.RecuperaObjetoBean;
import br.com.medinapassos.util.RelatoriosUtil;
import br.com.medinapassos.util.Sessao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.HibernateException;

/**
 *
 * @author JP1
 */
@ManagedBean
@ViewScoped
public class PrincialBean {

    private Sessao sessao;
    private String titulomensagem;
    private Recado recado;
    private Contato contato;
    private List<Recado> listaRecados;
    private List<Recado> listaRecadosEnviados;
    private List<Contato> listaContado;
    private MapaAplicacao linguagem;
    private Integer numerorecadosdiario;

    public PrincialBean() {
        try {
            IndexBean indexBean = (IndexBean) RecuperaObjetoBean.getObjectSession("indexBean");
            sessao = indexBean.getSessao();

            if (sessao == null) {
                FuncoesUtils.redirect("/pageserros/erro-autorizacao.jsf");
                return;
            }

            linguagem = Linguagem.carregarLiguagem(sessao.getContato().getIndioma());

        } catch (Exception e) {
            FuncoesUtils.redirect("/pageserros/erro.jsf");
        }
    }

    public void prepararNovoRecado() {
        contato = null;
        recado = new Recado();
        recado.setContatoremetenteid(sessao.getContato().getContatoid());
        recado.setQuem(sessao.getContato().getNome());
    }

    public void salvarRecado(String fecharTela, String abrirTela) {
        try {
            RecadoCTR recadoCTR = new RecadoCTR();

            if (recado.getContatoid() == null) {
                FuncoesUtils.enviarMensagemTela("Selecione um contato antes!", 2, fecharTela, abrirTela);
                return;
            }

            recado.setIndlido(Boolean.FALSE);
            recado.setHorario(new Date());
            recado.setQuem(sessao.getContato().getNome());
            recado.setContatoremetenteid(sessao.getContato().getContatoid());

            recadoCTR.salvar(recado);

            if (HibernateUtil.commitTransaction()) {
                listaRecadosEnviados = null;
                FuncoesUtils.enviarMensagemTela("Recado salvo com sucesso!", 1, fecharTela, abrirTela);
            } else {
                FuncoesUtils.enviarMensagemTela("Erro ao salvar recado!", 3, fecharTela, abrirTela);
            }
        } catch (Exception e) {
            FuncoesUtils.enviarMensagemTela("Erro ao salvar recado! ERRO: " + e.getLocalizedMessage(), 3, fecharTela, abrirTela);
        }
    }

    public void downloadPDFRelatorioRecado(ActionEvent actionEvent) throws HibernateException, JRException, SQLException, IOException {
        if (listaRecados != null && listaRecados.size() > 0) {
            Map parametros = new HashMap();
            parametros.put("id", sessao.getContato().getContatoid());
            parametros.put("nome", sessao.getContato().getNome());

            RelatoriosUtil relatoriosUtil = new RelatoriosUtil();

            relatoriosUtil.exportarPDF(parametros, "/relatorios/RecadosContato.jasper", "relatorioderecados-" + sessao.getContato().getLogin());
        }
    }

    public void prepararContatoRecado() {
        recado.setContato(contato);
        recado.setContatoid(contato.getContatoid());
    }

    public void marcarComoLido() {
        if (!recado.getIndlido()) {
            recado.setIndlido(true);
            RecadoCTR recadoCTR = new RecadoCTR();
            recadoCTR.salvar(recado);
        }
    }

    public void marcarComoNaoLido() {
        recado.setIndlido(false);
        RecadoCTR recadoCTR = new RecadoCTR();
        recadoCTR.salvar(recado);
    }

    public String getTitulomensagem() {
        return titulomensagem;
    }

    public void setTitulomensagem(String titulomensagem) {
        this.titulomensagem = titulomensagem;
    }

    public List<Recado> getListaRecados() {
        if (listaRecados == null) {
            RecadoCTR recadoCTR = new RecadoCTR();
            listaRecados = recadoCTR.listarContato(sessao.getContato().getContatoid());
        }
        return listaRecados;
    }

    public void setListaRecados(List<Recado> listaRecados) {
        this.listaRecados = listaRecados;
    }

    public List<Recado> getListaRecadosEnviados() {
        if (listaRecadosEnviados == null) {
            RecadoCTR recadoCTR = new RecadoCTR();
            listaRecadosEnviados = recadoCTR.listarRemetente(sessao.getContato().getContatoid());
            if (listaRecadosEnviados != null && getListaContado() != null) {
                FuncoesUtils.preencherContatoRemetenteRecado(listaRecadosEnviados, listaContado);
            }
        }
        return listaRecadosEnviados;
    }

    public void setListaRecadosEnviados(List<Recado> listaRecadosEnviados) {
        this.listaRecadosEnviados = listaRecadosEnviados;
    }

    public Recado getRecado() {
        return recado;
    }

    public void setRecado(Recado recado) {
        this.recado = recado;
    }

    public List<Contato> getListaContado() {
        if (listaContado == null) {
            ContatoCTR contatoCTR = new ContatoCTR();
            listaContado = contatoCTR.listar();
        }
        return listaContado;
    }

    public void setListaContado(List<Contato> listaContado) {
        this.listaContado = listaContado;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public MapaAplicacao getLinguagem() {
        return linguagem;
    }

    public void setLinguagem(MapaAplicacao linguagem) {
        this.linguagem = linguagem;
    }

    public Integer getNumerorecadosdiario() {
        if (numerorecadosdiario == null) {
            RecadoCTR recadoCTR = new RecadoCTR();
            numerorecadosdiario = recadoCTR.numerosrecadosdiario(sessao.getContato().getContatoid());
        }
        return numerorecadosdiario;
    }

    public void setNumerorecadosdiario(Integer numerorecadosdiario) {
        this.numerorecadosdiario = numerorecadosdiario;
    }

}
