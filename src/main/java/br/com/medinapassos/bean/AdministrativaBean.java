/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.bean;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.ctr.ContatoCTR;
import br.com.medinapassos.lazy.LazyContatoDataModel;
import br.com.medinapassos.util.FuncoesUtils;
import br.com.medinapassos.util.HibernateUtil;
import br.com.medinapassos.util.TarefaSingleton;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author JP1
 */
@ManagedBean
@ViewScoped
public class AdministrativaBean {

    private List<Contato> listaContato;
    private LazyDataModel<Contato> lazyModel;
    private Contato contato;

    public void iniciarTarefa() {
        TarefaSingleton.agendar();
    }

    public void excluirContato() {
        try {
            ContatoCTR contatoCTR = new ContatoCTR();

            contatoCTR.excluir(contato);

            if (HibernateUtil.commitTransaction()) {
                FuncoesUtils.enviarMensagemTela("Contato apagado com sucesso!", 1, "", "");
            } else {
                FuncoesUtils.enviarMensagemTela("Erro ao apagar contato!", 3, "", "");
            }
        } catch (Exception e) {
            FuncoesUtils.enviarMensagemTela("Erro ao apagar contato! ERRO: " + e.getLocalizedMessage(), 3, "", "");
        }
    }

    public void prepararAlterarContato() {
        ContatoCTR contatoCTR = new ContatoCTR();
        contato = contatoCTR.carregar(contato.getContatoid());
    }

    public void prepararNovoContato() {
        contato = new Contato();
    }

    public void salvarContato(String fecharTela, String abrirTela) {
        try {
            ContatoCTR contatoCTR = new ContatoCTR();

            if (contato.getContatoid() == null) {
                contato.setSenha(FuncoesUtils.converteSHA256(contato.getSenha()));
                contato.setLogin(contato.getLogin().toLowerCase());                
            } 
            
            if (!contatoCTR.verificarDisponivilidadeLogin(contato.getLogin(), contato.getContatoid())) {
                FuncoesUtils.enviarMensagemTela("Login já existe!", 2, fecharTela, abrirTela);
                return;
            }

            contatoCTR.salvar(contato);

            if (HibernateUtil.commitTransaction()) {
                FuncoesUtils.enviarMensagemTela("Contato salvo com sucesso!", 1, fecharTela, abrirTela);
            } else {
                FuncoesUtils.enviarMensagemTela("Erro ao salvar contato!", 3, fecharTela, abrirTela);
            }
        } catch (Exception e) {
            FuncoesUtils.enviarMensagemTela("Erro ao salvar contato! ERRO: " + e.getLocalizedMessage(), 3, fecharTela, abrirTela);
        }
    }

    public void home() {
        FuncoesUtils.redirect("");
    }

    public List<Contato> getListaContato() {
        if (listaContato == null) {
            ContatoCTR contatoCTR = new ContatoCTR();
            listaContato = contatoCTR.listar();
        }
        return listaContato;
    }

    public void setListaContato(List<Contato> listaContato) {
        this.listaContato = listaContato;
    }

    public LazyDataModel<Contato> getLazyModel() {
        if (lazyModel == null) {
            lazyModel = new LazyContatoDataModel();
        }
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Contato> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

}
