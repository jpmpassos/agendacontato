/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.bean;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.base.Recado;
import br.com.medinapassos.ctr.ContatoCTR;
import br.com.medinapassos.ctr.RecadoCTR;
import br.com.medinapassos.idioma.Linguagem;
import br.com.medinapassos.idioma.MapaAplicacao;
import br.com.medinapassos.util.FuncoesUtils;
import br.com.medinapassos.util.HibernateUtil;
import br.com.medinapassos.util.RecuperaObjetoBean;
import br.com.medinapassos.util.RelatoriosUtil;
import br.com.medinapassos.util.Sessao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.HibernateException;

/**
 *
 * @author JP1
 */
@ManagedBean
@ViewScoped
public class SecretariaBean {

    private Recado recado;
    private Contato contato;
    private Contato remetente;
    private List<Recado> listaRecados;
    private List<Contato> listaContado;

    public SecretariaBean() {

    }

    public void prepararNovoRecado() {
        contato = null;
        remetente = null;
        recado = new Recado();
    }

    public void salvarRecado(String fecharTela, String abrirTela) {
        try {
            RecadoCTR recadoCTR = new RecadoCTR();

            if (contato == null) {
                FuncoesUtils.enviarMensagemTela("Selecione um contato antes!", 2, fecharTela, abrirTela);
                return;
            }
            if (remetente == null) {
                FuncoesUtils.enviarMensagemTela("Selecione um remetente antes!", 2, fecharTela, abrirTela);
                return;
            }

            recado.setIndlido(Boolean.FALSE);
            recado.setHorario(new Date());
            recado.setQuem(remetente.getNome());
            recado.setContatoid(contato.getContatoid());
            recado.setContatoremetenteid(remetente.getContatoid());

            recadoCTR.salvar(recado);

            if (HibernateUtil.commitTransaction()) {
                FuncoesUtils.enviarMensagemTela("Recado salvo com sucesso!", 1, fecharTela, abrirTela);
            } else {
                FuncoesUtils.enviarMensagemTela("Erro ao salvar recado!", 3, fecharTela, abrirTela);
            }
        } catch (Exception e) {
            FuncoesUtils.enviarMensagemTela("Erro ao salvar recado! ERRO: " + e.getLocalizedMessage(), 3, fecharTela, abrirTela);
        }
    }

    public List<Recado> getListaRecados() {
        if (listaRecados == null) {
            RecadoCTR recadoCTR = new RecadoCTR();
            listaRecados = recadoCTR.listar();
        }
        return listaRecados;
    }

    public void setListaRecados(List<Recado> listaRecados) {
        this.listaRecados = listaRecados;
    }

    public Recado getRecado() {
        return recado;
    }

    public void setRecado(Recado recado) {
        this.recado = recado;
    }

    public List<Contato> getListaContado() {
        if (listaContado == null) {
            ContatoCTR contatoCTR = new ContatoCTR();
            listaContado = contatoCTR.listar();
        }
        return listaContado;
    }

    public void setListaContado(List<Contato> listaContado) {
        this.listaContado = listaContado;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Contato getRemetente() {
        return remetente;
    }

    public void setRemetente(Contato remetente) {
        this.remetente = remetente;
    }

}
