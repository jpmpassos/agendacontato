/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.bean;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.ctr.ContatoCTR;
import br.com.medinapassos.idioma.Linguagem;
import br.com.medinapassos.idioma.MapaAplicacao;
import br.com.medinapassos.util.FuncoesUtils;
import br.com.medinapassos.util.Sessao;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JP1
 */
@ManagedBean
@SessionScoped
public class IndexBean {

    private Sessao sessao;

    private String login;
    private String senha;

    public void logar() {
        ContatoCTR contatoCTR = new ContatoCTR();
        Contato contato = contatoCTR.recuperar(login.toLowerCase());
        if (contato != null && FuncoesUtils.converteSHA256(senha).equals(contato.getSenha())) {
            sessao = new Sessao(contato);
            FuncoesUtils.redirect("/pages/principal.jsf");
        } else {
            sessao = null;
            FuncoesUtils.enviarMensagemTela("Senha ou Login estão incorretos!", 3);
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Sessao getSessao() {
        return sessao;
    }

}
