/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  JP1
 * Created: 17/03/2018
 */

CREATE TABLE contatos (
    id                      INTEGER      NOT NULL AUTO_INCREMENT,
    login                   VARCHAR(140) NOT NULL,
    senha                   VARCHAR(255) NOT NULL,
    nome                    VARCHAR(140) NOT NULL,
    ramal                   INTEGER      NOT NULL,
    email                   VARCHAR(255) NULL,
    gti_created_at          DATETIME     ,
    gti_modified_at         DATETIME     ,
    INDEX IDX_contatos_nome (nome),
    INDEX IDX_contatos_ramal (ramal),
    PRIMARY KEY(id)
);

CREATE TABLE recados ( 
    id                INTEGER      NOT NULL AUTO_INCREMENT,
    idContato         INTEGER      NOT NULL,
    horario           TIMESTAMP    NOT NULL,
    quem              VARCHAR(140) NOT NULL COMMENT 'Quem deixou o recado',
    idRemetente       INTEGER      NOT NULL,
    texto             VARCHAR(255) NOT NULL,
    indLido           TINYINT(1)   NOT NULL DEFAULT 0,
    gti_created_at    DATETIME     ,
    gti_modified_at   DATETIME     ,
    INDEX IDX_recados_horario (idContato, horario DESC),
    INDEX FK_recados_contatos (idContato),
    CONSTRAINT FK_recados_contatos 
        FOREIGN KEY (idContato) REFERENCES contatos (id),
    PRIMARY KEY(id)
);
