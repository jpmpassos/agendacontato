/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  JP1
 * Created: 17/03/2018
 */

DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER  contatos_BEFORE_INSERT  
BEFORE INSERT ON  contatos  FOR EACH ROW
BEGIN
    set new.gti_created_at = current_timestamp(),
        new.gti_modified_at = current_timestamp();
 END //
DELIMITER;

DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER  contatos_BEFORE_UPDATE  
BEFORE UPDATE ON  contatos  FOR EACH ROW
BEGIN
    set new.gti_modified_at = current_timestamp();
END //
DELIMITER;

DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER  recados_BEFORE_INSERT  
BEFORE INSERT ON  recados  FOR EACH ROW
BEGIN
    set new.gti_created_at = current_timestamp(),
        new.gti_modified_at = current_timestamp();
 END //
DELIMITER;

DELIMITER //
CREATE DEFINER = CURRENT_USER TRIGGER  recados_BEFORE_UPDATE  
BEFORE UPDATE ON  recados  FOR EACH ROW
BEGIN
    set new.gti_modified_at = current_timestamp();
 END //
DELIMITER;