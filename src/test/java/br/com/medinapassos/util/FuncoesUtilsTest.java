/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.medinapassos.util;

import br.com.medinapassos.base.Contato;
import br.com.medinapassos.base.Recado;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JP1
 */
public class FuncoesUtilsTest {

    public FuncoesUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of converteSHA256 method, of class FuncoesUtils.
     */
    @Test
    public void testConverteSHA256() {
        String senha = "123456";
        String expResult = "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92";
        String result = FuncoesUtils.converteSHA256(senha);
        assertEquals(expResult, result);
    }

    /**
     * Test of preencherContatoRemetenteRecado method, of class FuncoesUtils.
     */
    @Test
    public void testPreencherContatoRemetenteRecado() {
        List<Recado> recados = new ArrayList<>();
        recados.add(new Recado(1, 2));
        recados.add(new Recado(2, 1));
        recados.add(new Recado(3, 2));
        recados.add(new Recado(4, 1));
        recados.add(new Recado(5, 4));
        recados.add(new Recado(6, 3));

        List<Contato> contatos = new ArrayList<>();
        contatos.add(new Contato(1));
        contatos.add(new Contato(2));
        contatos.add(new Contato(3));
        contatos.add(new Contato(4));

        FuncoesUtils.preencherContatoRemetenteRecado(recados, contatos);

        Comparator<Recado> crecado = new Comparator<Recado>() {
            @Override
            public int compare(Recado c1, Recado c2) {
                return c1.getRecadoid().compareTo(c2.getRecadoid());
            }
        };

        Collections.sort(recados, crecado);

        if (recados.get(0).getContato().getContatoid().intValue() != 2
                || recados.get(0).getContato().getContatoid().intValue() != 2
                || recados.get(1).getContato().getContatoid().intValue() != 1
                || recados.get(2).getContato().getContatoid().intValue() != 2
                || recados.get(3).getContato().getContatoid().intValue() != 1
                || recados.get(4).getContato().getContatoid().intValue() != 4
                || recados.get(5).getContato().getContatoid().intValue() != 3) {
            assertFalse(true);
        } else {
            assertFalse(false);
        }

    }

}
